# Rendu TD CICD

Lien du repo gitlab [https://gitlab.com/epsi6145509/cicd](https://gitlab.com/epsi6145509/cicd)

## Exercice 1 :

- Quelle est l'image utilisée par la CI ?
  - `Ruby:3.1`
- Quel est le nom du runner sur lequel le job est effectué ?

  - `blue-5.shared.runners-manager.gitlab.com`

- Rajoutez une étape dans la CI pour nous permettre d'afficher le numéro de version du python.

```yml
stages:
  - build

build-job:
  stage: build
  script:
    - echo "Hello World"
    - python -V
```

- Pourquoi est-ce que la commande ne marche-t-elle pas ?

  - Parce que l'image `ruby` n'a pas python d'installé

- Faites en sorte de changer l'image utilisée par le job par une image python et retestez.

```yml
image: python:alpine
stages:
  - build

build-job:
  stage: build
  script:
    - echo "Hello World"
    - python -V
```

## Exercice 2 :

Implémentez les étapes pour que la CI fonctionne.

```yml
stages:
  - test

test-job:
  stage: test
  image: python:alpine3.16
  script:
    - python src/test.py
```

- Qu'est ce que vous constatez au lancement des tests unitaires ?

  - Le test fail

- Trouvez la raison de cela, puis une solution pour que la CI s'exécute correctement.

  - Il faut changer la ligne 8 par
    ```python
    self.assertTrue(project.is_prime(5))
    ```

## Exercice 3

- Implémentez l'étape de lint.

  ```yml
  lint-job:
    stage: test
    image: python:alpine3.16
    script:
      - python -m pip install pylint
      - python -m pylint -d C0114 -d C0115 -d C0116 src/*.py #On ignore les trucs de docs
  ```

- Que constatez vous lors du lancement de la CI ?
  - Il manque le package pylint
- Comment faire pour régler ça ?

  - Il faut l'installer avec `pip install pylint`

- Quel est le problème avec le fait d'installer in package dans une CI ?
  - Il faut réinstaller le package à chaque run
- Comment pourriez vous résoudre ce problème ?
  - Il faudrait crée une image docker custom avec le package déjà installé

## Exercice 4 :

```yml
lint-job:
  stage: test
  image: python:alpine3.16
  script:
    - python -m pip install pylint
    - python -m pylint -d C0114 -d C0115 -d C0116 --output report.txt src/*.py #On ignore les truc de docs
  artifacts:
    paths:
      - report.txt
```

- Comment pouvez vous récupérer l'artéfact ?
  - Dans le job
- Combien de temps reste t-il disponible ?
  - Indéfiniment temps qu'on ne lance pas d'autre job

## Exercice 5 :

On ajoute un stage `report` pour pouvoir accèder aux artefacts du stage `test`.

```yml
stages:
  - test
  - report

[..]

echo-lint-job:
  stage: report
  script:
    - cat report.txt
```

## Exercice 6 :

Voila le `gitlab-ci.yml` final, j'utilise le registry intégré à gitlab

```yml
stages:
  - test
  - report
  - build

test-job:
  stage: test
  image: python:alpine3.16
  script:
    - python src/test.py

lint-job:
  stage: test
  image: python:alpine3.16
  script:
    - python -m pip install pylint
    - python -m pylint -d C0114 -d C0115 -d C0116 --output report.txt src/*.py #On ignore les truc de docs
  artifacts:
    paths:
      - report.txt

echo-lint-job:
  stage: report
  script:
    - cat report.txt

build_image_job:
  stage: build
  image: docker:20.10.10
  services:
    - docker:20.10.10-dind
  script:
    - echo $CI_REGISTRY_PASSWORD | docker login -u $CI_REGISTRY_USER $CI_REGISTRY --password-stdin
    - docker build -t $CI_REGISTRY_IMAGE .
    - docker push $CI_REGISTRY_IMAGE
```
