FROM python:alpine3.16

WORKDIR /app

COPY src/project.py project.py

CMD python project.py