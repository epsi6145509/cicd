import math


def is_prime(number):
    if number < 0:
        return 'Negative numbers are not allowed'

    if number <= 1:
        return False

    if number == 2:
        return True

    if number % 2 == 0:
        return False

    for i in range(2, int(math.sqrt(number)) + 1):
        if number % i == 0:
            return False
    return True


def cubic(number):
    return number * number * number


def say_hello(name):
    return "Hello, " + name


say_hello("world !")
